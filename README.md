# Tekton Challenge: Angular

Solución del Challenge asignado con Angular subido a firebase: [Enlace del challenge](https://tekton-challenge.firebaseapp.com/register)

#### Enlace

```link
  https://tekton-challenge.firebaseapp.com/register
```

## Bonus

- Folder Architecture
- Angular Material
- SCSS
- Custom pipes
- Custom validators
- Reactive form
- Linting
- Unit tests
- Coverage test
- Angular Universal

## Formulario

#### Campos Generales:

| Form Control      | Type     | Validators                                                           |
| :---------------- | :------- | :------------------------------------------------------------------- |
| `Names`           | `string` | **Required**. Caracteres alfabéticos, y especiales (acentos, ñ, ‘,.) |
| `Surnames`        | `string` | **Required**. Caracteres alfabéticos, y especiales (acentos, ñ, ‘,.) |
| `Nationality`     | `string` | **Required**. Caracteres alfabéticos                                 |
| `Document Type`   | `string` | **Required**. Campo de selección simple con 3 opciones               |
| `Document Number` | `string` | **Required**. Dependen del tipo de documento seleccionado            |

#### Campos del tipo de documento:

| Document Type | Type     | Validators                                            |
| :------------ | :------- | :---------------------------------------------------- |
| `DNI`         | `string` | **Required**. Caracteres numéricos, 8 como máximo     |
| `CE`          | `string` | **Required**. Caracteres alfanuméricos, 9 como máximo |
| `Pasaporte`   | `string` | **Required**. Caracteres numéricos, 9 como máximo     |

- Todos los campos muestran mensajes de error tanto cuando son requeridos y cuando son inválidos.
- Se muestra una indicación visual cuando los datos ingresados son válidos.

## Consideraciones Generales

Existen dos páginas:

```text
  /register
```

En esta página se puede registrar los datos de un pasajero.

- Permite agregar de manera dinámica hasta 4 pasajeros.
- Cada vez que se agrega un pasajero se emite un mensaje para indicar que la acción fue hecha satisfactoriamente.
- Cada vez que se registra un pasajero, se redirige a la página /summary para verificar los datos.

```text
  /summary
```

En esta página se puede ver los datos registrados anteriormente, se pueden modificarlos o eliminarlos.

- Cada vez que se modifica o elimina los datos de un pasajero, se emite un mensaje para indicar que la acción fue hecha satisfactoriamente.
- Solo cuando se quiere eliminar los datos de un pasajero se emite un diálogo de confirmación para verificar si se quiere eliminar o no.

## Linting

```bash
  ng lint
```

```text
  All files pass linting.
```

## Testing

```bash
  ng test --code-coverage
```

```text
  TOTAL: 92 SUCCESS
```

| Type         | Percent  | Description     |
| :----------- | :------- | :-------------- |
| `Statements` | `97.1%`  | **( 235/242 )** |
| `Branches`   | `83.33%` | **( 40/48 )**   |
| `Functions`  | `95.52%` | **( 64/67 )**   |
| `Lines`      | `96.91%` | **( 220/227 )** |
