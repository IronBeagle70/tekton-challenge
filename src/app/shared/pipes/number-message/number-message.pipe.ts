import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberMessage',
})
export class NumberMessagePipe implements PipeTransform {
  transform(passengersNumber: number): string {
    if (passengersNumber === 0) {
      return 'Aún no hay pasajeros registrados';
    } else {
      return `Pasajeros ${passengersNumber} de 4`;
    }
  }
}
