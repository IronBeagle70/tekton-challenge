import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'btnMessage',
})
export class BtnMessagePipe implements PipeTransform {
  transform(passengersNumber: number): string {
    if (passengersNumber === 0) {
      return 'Registrar';
    } else {
      return 'Seguir Registrando';
    }
  }
}
