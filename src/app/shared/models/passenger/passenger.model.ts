export class Passenger {
  names: string;
  surnames: string;
  nationality: string;
  documentType: string;
  documentNumber: string;

  constructor(
    names: string,
    surnames: string,
    nationality: string,
    documentType: string,
    documentNumber: string
  ) {
    this.names = names;
    this.surnames = surnames;
    this.nationality = nationality;
    this.documentType = documentType;
    this.documentNumber = documentNumber;
  }
}
