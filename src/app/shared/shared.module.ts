import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BtnMessagePipe } from '@shared/pipes/button-message/btn-message.pipe';
import { NumberMessagePipe } from '@shared/pipes/number-message/number-message.pipe';

import { ConfirmDialogComponent } from '@shared/components/confirm-dialog/confirm-dialog.component';
import { NotifierComponent } from '@shared/components/notifier/notifier.component';

import { FormErrorContainerComponent } from '@shared/components/form-extensions/form-error-container/form-error-container.component';
import { FormErrorMessageComponent } from '@shared/components/form-extensions/form-error-message/form-error-message.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@shared/material.module';

@NgModule({
  declarations: [
    BtnMessagePipe,
    NumberMessagePipe,
    ConfirmDialogComponent,
    NotifierComponent,
    FormErrorContainerComponent,
    FormErrorMessageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BtnMessagePipe,
    NumberMessagePipe,
    ConfirmDialogComponent,
    NotifierComponent,
    FormErrorContainerComponent,
    FormErrorMessageComponent,
    MaterialModule
  ]
})
export class SharedModule { }
