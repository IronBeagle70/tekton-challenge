import {
  AbstractControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

export const surnamesMustbeValid: ValidatorFn = (
  control: AbstractControl 
): ValidationErrors | null => {

  const surnamesValue = control.value;

  const surnamesRegex = new RegExp(/^[a-zA-ZñÑáéíóúÁÉÍÓÚüÜ\s.,']+$/);
  const surnamesPattern = surnamesRegex.test(surnamesValue);

  if(!surnamesValue){
    return { required : true}
  }

  if(!surnamesPattern){
    return { surnamesMustbeValid: true }
  }

  return null;
};

