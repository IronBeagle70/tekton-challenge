import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Passenger } from '@shared/models/passenger/passenger.model';
import { NotificationsService } from '../notifications/notifications.service';

@Injectable({
  providedIn: 'root',
})
export class StoreService {
  private passengers: Passenger[] = [];
  private passengersSubject = new BehaviorSubject<Passenger[]>(this.passengers);

  passengers$ = this.passengersSubject.asObservable();

  constructor(private notifierService: NotificationsService) {}

  addPassenger(passenger: Passenger) {
    if (this.passengers.length >= 4) {
      this.notifierService.showNotification(
        'No se puede registrar más de cuatro pasajeros',
        'Ok'
      );
      return;
    }
    this.passengers.push(passenger);
    this.passengersSubject.next(this.passengers);
    this.notifierService.showNotification(
      'Pasajero Registrado Satisfactoriamente',
      'Ok'
    );
  }

  getPassengers() {
    return this.passengers;
  }

  removePassenger(index: number) {
    this.passengers = this.passengers.filter((passenger, i) => i !== index);
    this.passengersSubject.next(this.passengers);
    this.notifierService.showNotification(
      'Pasajero Eliminado Satisfactoriamente',
      'Ok'
    );
  }
}
