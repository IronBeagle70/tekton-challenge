import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { StoreService } from '@shared/services/store/store.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent implements OnInit {
  passengersNumber = 0;

  constructor(private storeService: StoreService, private title: Title, private meta: Meta) {
    this.title.setTitle('Formulario de registro de Pasajeros');
    this.meta.updateTag({ name: 'description', content: 'Formulario para registrar hasta 4 pasajeros' });
    this.meta.updateTag({ name: 'og:description', content: 'Formulario de registro' });
  }

  ngOnInit(): void {
    this.storeService.passengers$.subscribe((passengers) => {
      this.passengersNumber = passengers.length;
    });
  }
}
