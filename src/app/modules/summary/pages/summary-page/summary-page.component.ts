import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Passenger } from '@shared/models/passenger/passenger.model';
import { StoreService } from '@shared/services/store/store.service';

@Component({
  selector: 'app-summary-page',
  templateUrl: './summary-page.component.html',
  styleUrls: ['./summary-page.component.scss'],
})
export class SummaryPageComponent implements OnInit {
  passengers: Passenger[] = [];

  constructor(private storeService: StoreService, private title: Title, private meta: Meta) {
    this.title.setTitle('Resumen de Pasajeros');
    this.meta.updateTag({ name: 'description', content: 'Visualizar , actualizar y eliminar los datos de los pasajeros' });
    this.meta.updateTag({ name: 'og:description', content: 'Resumen de Pasajeros' });
  }

  ngOnInit(): void {
    this.storeService.passengers$.subscribe((passenger) => {
      this.passengers = passenger;
    });
  }
}
