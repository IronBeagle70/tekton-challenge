import { NgModule } from '@angular/core';

import { SharedModule } from '@shared/shared.module';
import { SummaryRoutingModule } from './summary-routing.module';

import { SummaryPageComponent } from '@modules/summary/pages/summary-page/summary-page.component';
import { PassengersComponent } from '@modules/summary/components/passengers/passengers.component';
import { PassengerComponent } from '@modules/summary/components/passenger/passenger.component';
import { UpdateFormComponent } from '@modules/summary/components/update-form/update-form.component';

@NgModule({
  declarations: [
    SummaryPageComponent,
    PassengersComponent,
    PassengerComponent,
    UpdateFormComponent,
  ],
  imports: [
    SharedModule,
    SummaryRoutingModule
  ]
})
export class SummaryModule { }
