import { Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'tekton-challenge';
  constructor(private titleMeta: Title, private meta: Meta) {
    this.titleMeta.setTitle('Tekton Airlines');
    this.meta.addTag({ name: 'author', content: 'Alvaro Mendoza' });
    this.meta.addTag({ name: 'description', content: 'Formulario para registrar los datos de hasta 4 pasajeros' });
    this.meta.addTag({ name: 'og:title', content: 'Tekton Airlines' });
    this.meta.addTag({ name: 'og:description', content: 'Formulario de registro de pasajeros' });
    this.meta.addTag({ name: 'og:image', content: 'https://gitlab.com/IronBeagle70/tekton-challenge/-/raw/main/src/assets/images/TKlogo.png' });
  }
}
